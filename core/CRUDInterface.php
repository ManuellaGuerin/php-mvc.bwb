<?php

interface CRUDInterface
{
    // Méthodes
    public function retrieve($id);
    public function update($id);
    public function delete($id);
    public function create($tableauAssos);
}

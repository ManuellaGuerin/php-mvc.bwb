<?php

interface RepositoryInterface
{
    // Méthodes
    public function getAll($table);
    public function getAllBy($assosWhereAnd);
}

<?php


abstract class DAO implements CRUDInterface, RepositoryInterface
{
    // Propriétés 
    protected $_pdo; // "Voilà comment on crée un PDO" Honnêtement, il m'est cauchemardesque celui-là. ...un peu moins today

    // Contructeur
    public function __construct()
    {
        // les éléments dont j'aurai besoin pour ma méthode
        $fichierJson = file_get_contents('./config/databaseCar.json');
        $jsonDecoder = json_decode($fichierJson);
        // var_dump($jsonDecoder);
        $dsn = "$jsonDecoder->driver:$jsonDecoder->host;port=3306;dbname=BDD;charset=utf8";
        echo "$jsonDecoder->driver:$jsonDecoder->host;port=3306;dbname=BDD;charset=utf8";

        $this->_pdo = new PDO($dsn, $jsonDecoder->username, $jsonDecoder->password);
    }


    // Méthodes implémentées depuis CRUD: Manipulation d'une entité unique
    abstract public function retrieve($id);
    abstract public function update($id);
    abstract public function delete($id);
    abstract public function create($tableauAssos);

    // Méthodes implémentées depuis Repository: Manipulation de collections(tableaux, listes)
    public function getAll($table) //version dynamique
    {
        $requeteSQL = 'SELECT * FROM ' . $table;
        $reponseSQL = $this->_pdo->query($requeteSQL);
        var_dump($reponseSQL);
        return $reponseSQL;
    }
    public function getAllBy($assosWhereAnd) // code en dur
    {
        $requeteSQL = "SELECT * FROM thecar WHERE brand='Citroen'";
        $assosWhereAnd = $this->_pdo->query($requeteSQL);
        var_dump($assosWhereAnd);
        return $assosWhereAnd;
    }

}
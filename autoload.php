<?php

spl_autoload_register( function ($nomFichier) {
    if (file_exists ('./core/' . $nomFichier. '.php')) {
        include './core/'.$nomFichier.'.php';
    } elseif (file_exists ('./controllers/'. $nomFichier. '.php')) {
        include './controllers/'. $nomFichier. '.php';
    } elseif (file_exists ('./config/'. $nomFichier. '.php')) {
        include './config/'. $nomFichier. '.php';
    } elseif (file_exists ('./dao/'.$nomFichier. '.php')) {
        include './dao/'. $nomFichier. '.php';
    } elseif (file_exists ('./models/'. $nomFichier. '.php')) {
        include './models/'. $nomFichier. '.php';
    } elseif (file_exists ( './vendors/'. $nomFichier. '.php')) {
        include './vendors/'. $nomFichier. '.php';
    } elseif (file_exists ( './views/'. $nomFichier. '.php')) {
        include './views/'. $nomFichier. '.php';
    }
})

?>